package com.example.safeauto.RaiseClaim.views;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.safeauto.ClaimReview.views.ClaimReviewActivity;
import com.example.safeauto.R;
import com.example.safeauto.RaiseClaim.Model.ImageModel;
import com.example.safeauto.RaiseClaim.adapter.ImageRecyclerAdapter;
import com.example.safeauto.RaiseClaim.adapter.SelectDriverAdapter;
import com.example.safeauto.RaiseClaim.adapter.SelectVehicleAdapter;
import com.example.safeauto.UnlistedDrivers.views.unlistedDriverActivity;
import com.example.safeauto.main.views.MainActivity;
import com.example.safeauto.utils.Constants;
import com.example.safeauto.utils.FontAwesome;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;

public class RaiseAClaimActivity extends MainActivity implements LocationListener {
    RelativeLayout layout_main, cnfrminfo_lay;
    View view_child, view;
    RecyclerView selctvh_rcyclvw, selctdrv_rcyclvw, image_rcyclvw;
    SelectVehicleAdapter selectVehicleAdapter;
    SelectDriverAdapter selectDriverAdapter;
    SegmentedButtonGroup segmentedButtonGroup;
    CheckBox currenttime_cb, currentloc_cb, cb1, cb2, cb3, cb4;
    TextView date_textview, time_textview, scrn_title;
    EditText address_edtxtvw;
    Button choosefile_btn, submit_btn, adddrv_btn;
    FontAwesome camera_txtvw;
    ArrayList<ImageModel> imageModelArrayList;
    ImageRecyclerAdapter imageRecyclerAdapter;
    String provider;
    LocationManager locationManager;
    public static boolean Flag = false;
    Context context;
    SegmentedButton seg_btn1;



//    RadioGroup radioGroup1,radioGroup2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout_main = (RelativeLayout) findViewById(R.id.rellay_main);
        view_child = getLayoutInflater().inflate(R.layout.activity_raise_aclaim, null);
        context = getApplicationContext();

        initView();

        layout_main.addView(view_child);
    }

    public void initView() {

        //screen title modification
        findViewById(R.id.sa_logo).setVisibility(View.GONE);
        scrn_title = (TextView) findViewById(R.id.scrntitle_txtvw);
        scrn_title.setVisibility(View.VISIBLE);
        scrn_title.setText("Report a Claim");

        layout_main.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        submit_btn = (Button) view_child.findViewById(R.id.submit_btn);

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Button confirminfo_btn;

                cnfrminfo_lay = (RelativeLayout) findViewById(R.id.confrm_relay);
                cnfrminfo_lay.setVisibility(View.VISIBLE);

                view = getLayoutInflater().inflate(R.layout.confirminfo_layout, null);
                confirminfo_btn = view.findViewById(R.id.confirminfo_btn);

                confirminfo_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirminfo_btn.setVisibility(View.GONE);
                        Intent i = new Intent(RaiseAClaimActivity.this, ClaimReviewActivity.class);
                        startActivity(i);
                    }
                });

                cnfrminfo_lay.addView(view);
            }
        });

        whtHappend_Init();

        selctvh_rcyclvw = (RecyclerView) view_child.findViewById(R.id.slectvh_recyclvw);
        selectVehicle_Init();

        timeLocation_Init();

        selctdrv_rcyclvw = (RecyclerView) view_child.findViewById(R.id.slectdrv_recyclvw);
        selectDriver_Init();

        needAssistance_Init();

        passengerInfo_Init();

        uploadPhoto_Init();

    }

    public void whtHappend_Init() {
        final RadioGroup radioGroup1 = (RadioGroup) view_child.findViewById(R.id.radiogroup1);
        final RadioGroup radioGroup2 = (RadioGroup) view_child.findViewById(R.id.radiogroup2);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i != -1)
                    radioGroup2.check(-1);
                switch (i) {
                    case R.id.radio1_1:
                        // do operations specific to this selection
                        Flag = true;
                        findViewById(R.id.radio1_1).setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                        findViewById(R.id.radio1_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_1).setSelected(false);
                        showCustomDialog();
                        findViewById(R.id.radio1_1).setSelected(true);
                        break;
                    case R.id.radio1_2:
                        // do operations specific to this selection
                        Flag = false;
                        findViewById(R.id.radio1_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_2).setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                        findViewById(R.id.radio1_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        break;
                    case R.id.radio1_3:
                        // do operations specific to this selection
                        Flag = false;
                        findViewById(R.id.radio1_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_3).setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                        findViewById(R.id.radio2_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        break;
                }
                selectVehicle_Init();


                Toast.makeText(RaiseAClaimActivity.this, "position : " + i, Toast.LENGTH_SHORT).show();
            }
        });

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i != -1)
                    radioGroup1.check(-1);
                switch (i) {
                    case R.id.radio2_1:
                        // do operations specific to this selection
                        Flag = true;
                        findViewById(R.id.radio1_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_1).setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                        findViewById(R.id.radio2_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        break;
                    case R.id.radio2_2:
                        // do operations specific to this selection
                        Flag = true;
                        findViewById(R.id.radio1_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_2).setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                        findViewById(R.id.radio2_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        break;
                    case R.id.radio2_3:
                        // do operations specific to this selection
                        Flag = false;
                        findViewById(R.id.radio1_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio1_3).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_1).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_2).setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                        findViewById(R.id.radio2_3).setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                        break;
                }
                selectVehicle_Init();
            }
        });
    }

    private void showCustomDialog() {

        Button donebtn;
        final LinearLayout linlay1,linlay2,linlay3;

        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.select_car_dialog, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        donebtn = (Button) dialogView.findViewById(R.id.donebtn);
        linlay1 = (LinearLayout) dialogView.findViewById(R.id.linlay_1);
        linlay2 = (LinearLayout) dialogView.findViewById(R.id.linlay_2);
        linlay3 = (LinearLayout) dialogView.findViewById(R.id.linlay_3);

        linlay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linlay1.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                linlay2.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                linlay3.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            }
        });

        linlay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linlay1.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                linlay2.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                linlay3.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            }
        });

        linlay3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linlay1.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                linlay2.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                linlay3.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
            }
        });

        donebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        //finally creating the alert dialog and displaying it

        alertDialog.show();
    }

    public void selectVehicle_Init() {
        Constants.vehicle_pojoArrayList.clear();
        Constants.addDatatoVehiclePojo();
        selectVehicleAdapter = new SelectVehicleAdapter(Constants.vehicle_pojoArrayList, RaiseAClaimActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RaiseAClaimActivity.this);
        selctvh_rcyclvw.setLayoutManager(mLayoutManager);
        selctvh_rcyclvw.setAdapter(selectVehicleAdapter);
    }

    public void timeLocation_Init() {

        date_textview = (TextView) view_child.findViewById(R.id.date_txtvw);
        time_textview = (TextView) view_child.findViewById(R.id.time_txtvw);
        address_edtxtvw = (EditText) view_child.findViewById(R.id.address_edtxtvw);

        currenttime_cb = (CheckBox) view_child.findViewById(R.id.cb_jstnw);
        currentloc_cb = (CheckBox) view_child.findViewById(R.id.cb_curntloc);

        final Calendar myCalendar = Calendar.getInstance();


        //Datepicker to set date
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "mm/dd/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                date_textview.setText(sdf.format(myCalendar.getTime()));
            }

        };


        date_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currenttime_cb.setChecked(false);
                date_textview.setText("mm/dd/yyyy");
                time_textview.setText("--:--:--");

                new DatePickerDialog(RaiseAClaimActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        time_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Use the current time as the default values for the picker
                int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
                int minute = myCalendar.get(Calendar.MINUTE);
                final int sec = myCalendar.get(Calendar.SECOND);
                // Create a new instance of TimePickerDialog
                final TimePickerDialog timePickerDialog = new TimePickerDialog(RaiseAClaimActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time_textview.setText(selectedHour + ":" + selectedMinute + ":" + sec);
                    }
                }, hour, minute, true);//Yes 24 hour time
                timePickerDialog.setTitle("Select Time");
                timePickerDialog.show();
            }
        });

        currenttime_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                    String[] strDate = sdf.format(c.getTime()).split(" ");
                    date_textview.setText(strDate[0]);
                    time_textview.setText(strDate[1]);
                } else if (!((CheckBox) v).isChecked()) {
                    date_textview.setText("mm/dd/yyyy");
                    time_textview.setText("--:--:--");
                }
            }
        });

        currentloc_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    setCurrentLocation();
                }
            }
        });


    }

    public void setCurrentLocation() {

        try {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);

        } catch (SecurityException e) {
            Log.i(Constants.Tag, "setCurrentLocation Exception " + e);
        }

    }

    public String getAddress(String lat, String lon) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        String ret = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("Address:\n");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                ret = strReturnedAddress.toString();
            } else {
                ret = "No Address returned!";
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.i(Constants.Tag, "getAddress Exception " + e);
            ret = "Can't get Address!";
        }
        return ret;
    }

    public void selectDriver_Init() {
        Constants.driver_pojoArrayList.clear();
        Constants.addDataToDriverPojo();
        selectDriverAdapter = new SelectDriverAdapter(Constants.driver_pojoArrayList, RaiseAClaimActivity.this);
        RecyclerView.LayoutManager LayoutManager = new LinearLayoutManager(RaiseAClaimActivity.this);
        selctdrv_rcyclvw.setLayoutManager(LayoutManager);
        selctdrv_rcyclvw.setAdapter(selectDriverAdapter);

        adddrv_btn = view_child.findViewById(R.id.adddrv_btn);
        adddrv_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RaiseAClaimActivity.this, unlistedDriverActivity.class);
                startActivity(intent);
            }
        });


    }

    public void needAssistance_Init() {
        cb1 = (CheckBox) view_child.findViewById(R.id.checkbox0);
        cb2 = (CheckBox) view_child.findViewById(R.id.checkbox1);
        cb3 = (CheckBox) view_child.findViewById(R.id.checkbox2);
        cb4 = (CheckBox) view_child.findViewById(R.id.checkbox3);
    }

    public void onAssistanceCheckboxClicked(View view) {

        final LinearLayout cb_linlay1 = (LinearLayout) view_child.findViewById(R.id.cb_linlay1);
        final LinearLayout cb_linlay2 = (LinearLayout) view_child.findViewById(R.id.cb_linlay2);
        final LinearLayout cb_linlay3 = (LinearLayout) view_child.findViewById(R.id.cb_linlay3);
        final LinearLayout cb_linlay4 = (LinearLayout) view_child.findViewById(R.id.cb_linlay4);
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();
        // Check which checkbox was clicked
        switch (view.getId()) {
            case R.id.checkbox0:
                if (checked) {
                    cb3.setChecked(true);
                    cb4.setChecked(true);
                    cb_linlay1.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                    cb_linlay3.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                    cb_linlay4.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                    cb_linlay2.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                } else {
                    cb3.setChecked(false);
                    cb4.setChecked(false);
                    cb_linlay1.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                    cb_linlay2.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                    cb_linlay3.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                    cb_linlay4.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                }
                break;
            case R.id.checkbox1:
                if (checked) {
                    cb_linlay2.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                } else {
                    cb_linlay2.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                }
                break;
            case R.id.checkbox2:
                if (checked) {
                    cb_linlay3.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                } else {
                    cb_linlay3.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                }
                break;
            case R.id.checkbox3:
                if (checked) {
                    cb_linlay4.setBackground(getResources().getDrawable(R.drawable.blue_borderbox));
                } else {
                    cb_linlay4.setBackground(getResources().getDrawable(R.drawable.white_borderbox));
                }
                break;
        }
    }

    public void passengerInfo_Init() {
        segmentedButtonGroup = (SegmentedButtonGroup) view_child.findViewById(R.id.segmentbtngrp);
        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
//                seg_btn1.setTextColorOnSelection(getResources().getColor(R.color.colorBlack));
                Toast.makeText(RaiseAClaimActivity.this, "postion " + position + " segmentedButtonGroup.getSelectorColor()" + segmentedButtonGroup.getSelectorColor(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void uploadPhoto_Init() {
        choosefile_btn = (Button) view_child.findViewById(R.id.choosefile_btn);
        camera_txtvw = (FontAwesome) view_child.findViewById(R.id.camera_icon);
        image_rcyclvw = (RecyclerView) view_child.findViewById(R.id.image_recyclvw);
        imageModelArrayList = new ArrayList<>();

        choosefile_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.REQUEST_PICK_IMAGE);

                image_rcyclvw.setVisibility(View.VISIBLE);
            }
        });

        camera_txtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, Constants.REQUEST_CAMERA);
                image_rcyclvw.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
           /* tmap photo = (Bitmap) data.getExtras().get("data");
          selectedImageWork.setAlpha(1f);*//*
            Bi*//*  selectedImage.setImageBitmap(photo);*//*
            Matrix mat = new Matrix();
            mat.postRotate(Integer.parseInt("270"));
            Bitmap bMapRotate = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), mat, true);
//            selectedImageWork.setImageBitmap(bMapRotate);
            ImageModel imageModel = new ImageModel(); // the model between activity and adapter
            imageModel.setPhoto(bMapRotate.getNinePatchChunk());  // here i pass the photo
            imageModelArrayList.add(imageModel);
            setImageAdapter(imageModelArrayList);*/
            onCaptureImageResult(data);

        } else if (requestCode == Constants.REQUEST_PICK_IMAGE && resultCode == RESULT_OK && data != null) {


            /*Uri selectedImageURI = data.getData();
            ImageModel imageModel = new ImageModel(); // the model between activity and adapter
            imageModel.setPhoto(convertImage2Base64());  // here i pass the photo
            imageModelArrayList.add(imageModel);

            imageRecyclerAdapter.updateList(imageModelArrayList); // add this

            imageRecyclerAdapter.notifyDataSetChanged();
            setImageAdapter(imageModelArrayList);*/

            onSelectFromGalleryResult(data);
        } else {
            image_rcyclvw.setVisibility(View.GONE);
        }
    }

    // this method will convert the image to base64
    /*public byte[] convertImage2Base64() {
        Bitmap bitmap = ((BitmapDrawable) selectedImageWork.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = Base64.decode(stream.toByteArray(), Base64.DEFAULT);
        return image;
    }*/

    public void setImageAdapter(ArrayList<ImageModel> imageModelArrayList) {
        imageRecyclerAdapter = new ImageRecyclerAdapter(RaiseAClaimActivity.this, imageModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RaiseAClaimActivity.this);
        ((LinearLayoutManager) mLayoutManager).setOrientation(LinearLayoutManager.HORIZONTAL);
        image_rcyclvw.setLayoutManager(mLayoutManager);
        image_rcyclvw.setAdapter(imageRecyclerAdapter);
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                ImageModel imageModel = new ImageModel(); // the model between activity and adapter
                imageModel.setPhoto(bm);  // here i pass the photo
                imageModelArrayList.add(imageModel);
                setImageAdapter(imageModelArrayList);

            } catch (Exception e) {
                Log.e(Constants.Tag, "onSelectFromGalleryResult error : " + e);
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        Log.e(Constants.Tag, "onCaptureImageResult error : " + thumbnail);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        /*File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            Log.e(Constants.Tag,"onCaptureImageResult error : "+destination.getAbsolutePath());
        }  catch (Exception e) {
            Log.e(Constants.Tag,"onCaptureImageResult error : "+e);
        }*/
        ImageModel imageModel = new ImageModel(); // the model between activity and adapter
        imageModel.setPhoto(thumbnail);  // here i pass the photo
        imageModelArrayList.add(imageModel);
        setImageAdapter(imageModelArrayList);

//        ivImage.setImageBitmap(thumbnail);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(Constants.Tag, "locationManager getLatitude : " + location.getLatitude() + "getLongitude : " + location.getLongitude());
        Log.i(Constants.Tag, "address is : " + getAddress("" + location.getLatitude(), "" + location.getLongitude()));
        address_edtxtvw.setText(getAddress("" + location.getLatitude(), "" + location.getLongitude()));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
