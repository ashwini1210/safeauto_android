package com.example.safeauto.PolicyDetails.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.safeauto.PolicyDetails.model.Coverage_POJO;
import com.example.safeauto.R;
import com.example.safeauto.utils.Constants;

import java.util.ArrayList;

public class CoverageRecycler_Adapter extends RecyclerView.Adapter<CoverageRecycler_Adapter.ViewHolder> {
    View view;
    private ArrayList<Coverage_POJO> coverage_pojoArrayList;
    private Context mContext;

    public CoverageRecycler_Adapter(ArrayList<Coverage_POJO> coverage_pojoArrayList, Context mContext) {
        this.coverage_pojoArrayList = coverage_pojoArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Coverage_POJO pojo = coverage_pojoArrayList.get(i);
        if (!pojo.getCv_accdnt().isEmpty()) {
            Constants.RFlag = true;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_coverages, viewGroup, false);
        }
        else /*if ( pojo.getCv_accdnt().contains(""))*/{
            Constants.RFlag = false;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_coverage_recycle2, viewGroup, false);

        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Coverage_POJO pojo = coverage_pojoArrayList.get(i);
        if (Constants.RFlag == true){
            viewHolder.cv_lib.setText(pojo.getCv_lib());
            viewHolder.cv_accdnt.setText(pojo.getCv_accdnt());
            viewHolder.cv_persn.setText(pojo.getCv_persn());
        }else if (Constants.RFlag == false){
            viewHolder.cv_lib.setText(pojo.getCv_lib());
            viewHolder.cv_accdnt.setText(pojo.getCv_accdnt());
        }

    }

    @Override
    public int getItemCount() {
        return coverage_pojoArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView cv_lib, cv_accdnt, cv_persn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            if (Constants.RFlag == true) {
                cv_lib = itemView.findViewById(R.id.cv_libtxt);
                cv_accdnt = itemView.findViewById(R.id.cv_accdnttxt);
                cv_persn = itemView.findViewById(R.id.cv_persntxt);
            }else if (Constants.RFlag == false){
                cv_lib = itemView.findViewById(R.id.cv_libtxt);
                cv_accdnt = itemView.findViewById(R.id.cv_accdnttxt);
            }

        }

        @Override
        public void onClick(View v) {

        }
    }
}
