package com.example.safeauto.PolicyDetails.views;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.safeauto.PolicyDetails.adapter.Fragment_Adapter;
import com.example.safeauto.R;
import com.example.safeauto.main.views.MainActivity;
import com.example.safeauto.utils.Constants;

public class PolicyDetialsActivity extends MainActivity {

    View view_child;
    Fragment_Adapter driverFragment_adapter;
    ViewPager viewPager;
    TextView scrn_title;
    RelativeLayout layout_main;

    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout_main = (RelativeLayout) findViewById( R.id.rellay_main);
        view_child = getLayoutInflater().inflate( R.layout.activity_policy_detials, null);

       /* Bundle bundle = getIntent().getExtras();
        String policyno = bundle.getString("Policy_No");*/

        findViewById(R.id.sa_logo).setVisibility(View.GONE);
        scrn_title = (TextView) findViewById(R.id.scrntitle_txtvw);
        scrn_title.setVisibility(View.VISIBLE);
        scrn_title.setText("Policy # \n "+ Constants.POLICY_NO);
        layout_main.setBackground(getResources().getDrawable(R.drawable.main_bgimg));

        viewPager = view_child.findViewById(R.id.policydetails_vpger);
        tabLayout = view_child.findViewById(R.id.tablayout);
        driverFragment_adapter = new Fragment_Adapter(getSupportFragmentManager());
        viewPager.setAdapter(driverFragment_adapter);
        tabLayout.setupWithViewPager(viewPager);
        layout_main.addView(view_child);

    }
}
