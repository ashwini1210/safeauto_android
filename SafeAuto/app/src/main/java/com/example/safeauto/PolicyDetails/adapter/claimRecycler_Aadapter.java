package com.example.safeauto.PolicyDetails.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.safeauto.PolicyDetails.model.Claim_POJO;
import com.example.safeauto.R;
import java.util.ArrayList;

public class claimRecycler_Aadapter extends RecyclerView.Adapter<claimRecycler_Aadapter.ViewHolder> {

    ArrayList<Claim_POJO> claim_pojoArrayList;

    public claimRecycler_Aadapter(ArrayList<Claim_POJO> claim_pojoArrayList) {
        this.claim_pojoArrayList = claim_pojoArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_claims, viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Claim_POJO pojo = claim_pojoArrayList.get(i);
        viewHolder.clm_no.setText(pojo.getClm_no());
        viewHolder.clm_date.setText(pojo.getClm_date());
        viewHolder.clm_status.setText(pojo.getClm_status());
    }

    @Override
    public int getItemCount() {
        Log.i("ArraySize"," : "+claim_pojoArrayList.size());
        return claim_pojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView clm_no, clm_date, clm_status;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            clm_no = (TextView) itemView.findViewById(R.id.clm_no);
            clm_date = (TextView) itemView.findViewById(R.id.clm_date);
            clm_status = (TextView) itemView.findViewById(R.id.clm_status);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
