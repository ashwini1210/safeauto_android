package com.example.safeauto.PolicyDetails.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.safeauto.PolicyDetails.adapter.driverRecycler_Adapter;
import com.example.safeauto.PolicyDetails.model.Driver_POJO;
import com.example.safeauto.R;
import com.example.safeauto.utils.Constants;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DriversFragment extends Fragment {
    View rootview;
    RecyclerView recyclerView;
    driverRecycler_Adapter driverRecycler_adapter;
    ArrayList<Driver_POJO> driver_pojoArrayList =  new ArrayList<>();


    public DriversFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_drivers, container, false);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.drv_recyclevw);
        Constants.addDataToDriverPojo();
        driverRecycler_adapter = new driverRecycler_Adapter(Constants.driver_pojoArrayList, getActivity().getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(driverRecycler_adapter);
        return rootview;
    }

    public static DriversFragment newInstance(int position) {

        Bundle args = new Bundle();
        DriversFragment fragment = new DriversFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
