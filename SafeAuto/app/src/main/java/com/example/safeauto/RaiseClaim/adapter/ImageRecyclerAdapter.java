package com.example.safeauto.RaiseClaim.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.safeauto.R;
import com.example.safeauto.RaiseClaim.Model.ImageModel;

import java.util.ArrayList;

public class ImageRecyclerAdapter extends RecyclerView.Adapter<ImageRecyclerAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<ImageModel> imageModelArrayList;

    public ImageRecyclerAdapter(Context mContext, ArrayList<ImageModel> imageModelArrayList) {
        this.mContext = mContext;
        this.imageModelArrayList = imageModelArrayList;
    }

    @NonNull
    @Override
    public ImageRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_horizontal_image, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageRecyclerAdapter.ViewHolder viewHolder, int i) {
        ImageModel imageModellist = imageModelArrayList.get(i);
        /*Glide.with(mContext).load(imageModellist.getPhoto())
                .into(viewHolder.picture);*/
        viewHolder.picture.setImageBitmap(imageModellist.getPhoto());
    }

    @Override
    public int getItemCount() {
        return imageModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView picture;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            picture = (ImageView) itemView.findViewById(R.id.imgvw);

        }
    }

    public void updateList(ArrayList<ImageModel> imageModelArrayList) {
        this.imageModelArrayList = imageModelArrayList;
        notifyDataSetChanged();
    }
}
