package com.example.safeauto.PolicyDetails.model;

public class Driver_POJO {
    private String drv_name, drv_dob, drv_license;
    private int drv_imgsrc;

    public Driver_POJO(String drv_name, String drv_dob, String drv_license, int drv_imgsrc) {
        this.drv_name = drv_name;
        this.drv_dob = drv_dob;
        this.drv_license = drv_license;
        this.drv_imgsrc = drv_imgsrc;
    }

    public String getDrv_name() {
        return drv_name;
    }

    public void setDrv_name(String drv_name) {
        this.drv_name = drv_name;
    }

    public String getDrv_dob() {
        return drv_dob;
    }

    public void setDrv_dob(String drv_dob) {
        this.drv_dob = drv_dob;
    }

    public String getDrv_license() {
        return drv_license;
    }

    public void setDrv_license(String drv_license) {
        this.drv_license = drv_license;
    }

    public int getDrv_imgsrc() {
        return drv_imgsrc;
    }

    public void setDrv_imgsrc(int drv_imgsrc) {
        this.drv_imgsrc = drv_imgsrc;
    }
}
