package com.example.safeauto.PolicyDetails.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.safeauto.PolicyDetails.views.ClaimsFragment;
import com.example.safeauto.PolicyDetails.views.CoveragesFragment;
import com.example.safeauto.PolicyDetails.views.DriversFragment;
import com.example.safeauto.PolicyDetails.views.VehiclesFragment;

public class Fragment_Adapter extends FragmentPagerAdapter {

    private String fragmentName[] = {" Vehicles "," Drivers ","Coverages"," Claims "};

    public Fragment_Adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        switch (i) {
            case 0:
                fragment =  VehiclesFragment.newInstance(0);
                break;
            case 1:
                fragment =  DriversFragment.newInstance(1);
                break;
            case 2:
                fragment =  CoveragesFragment.newInstance(2);
                break;
            case 3:
                fragment =  ClaimsFragment.newInstance(3);
                break;
            default:
                fragment =  VehiclesFragment.newInstance(0);
                break;
        }

        return fragment;

    }

    @Override
    public int getCount() {
        return fragmentName.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentName[position];
    }
}
