package com.example.safeauto.PolicyDetails.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.safeauto.PolicyDetails.adapter.CoverageRecycler_Adapter;
import com.example.safeauto.PolicyDetails.model.Coverage_POJO;
import com.example.safeauto.R;
import com.example.safeauto.utils.Constants;
import com.example.safeauto.utils.FontAwesome;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CoveragesFragment extends Fragment {

    View rootview;
    RecyclerView recyclerView,recyclerView1;
    ArrayList<Coverage_POJO> coverage_pojoArrayList = new ArrayList<>();
    ArrayList<Coverage_POJO> coverage_pojoArrayList1 = new ArrayList<>();
    CoverageRecycler_Adapter coverageRecycler_adapter;

    public CoveragesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview =  inflater.inflate(R.layout.fragment_coverages, container, false);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recycle_vw1);
        recyclerView1= (RecyclerView) rootview.findViewById(R.id.recycle_vw2);
        addDataToPojo();

        Constants.RFlag = true;
        coverageRecycler_adapter = new CoverageRecycler_Adapter(coverage_pojoArrayList, getActivity().getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext()) ;
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(coverageRecycler_adapter);

        Constants.RFlag = false;
        coverageRecycler_adapter = new CoverageRecycler_Adapter(coverage_pojoArrayList1, getActivity().getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity().getApplicationContext()) ;
        recyclerView1.setLayoutManager(mLayoutManager1);
        recyclerView1.setAdapter(coverageRecycler_adapter);

        return rootview;
    }

    public static CoveragesFragment newInstance(int position) {

        Bundle args = new Bundle();

        CoveragesFragment fragment = new CoveragesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void addDataToPojo(){
        coverage_pojoArrayList.clear();
        Coverage_POJO c,c1,c2,c3,c4,c5;
        c = new Coverage_POJO("Bodily Injury Liability", "$10,000.00", "$10,000.00");
        c1 = new Coverage_POJO("Property Damage Liability", "$10,000.00", "-");
        c2 = new Coverage_POJO("Uninsured - Bodily Injury", "$10,000.00", "$10,000.00");
        c3 = new Coverage_POJO("Uninsured - Property Damage", "$10,000.00", "-");
        c4 = new Coverage_POJO("Personal Injury Protection", "$10,000.00", "-");

        coverage_pojoArrayList.add(c);
        coverage_pojoArrayList.add(c1);
        coverage_pojoArrayList.add(c2);
        coverage_pojoArrayList.add(c3);
        coverage_pojoArrayList.add(c4);

        for (int i=0;i<4;i++) {
            c5 = new Coverage_POJO("Comprehensive", "", getString(R.string.icon_check));
            coverage_pojoArrayList1.add(c5);
        }

    }
}
