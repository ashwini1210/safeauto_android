package com.example.safeauto.DashBoard.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.safeauto.DashBoard.model.DashBoardVehicle_POJO;
import com.example.safeauto.R;
import com.example.safeauto.utils.Constants;

import java.util.ArrayList;

public class RecyclerViewChildAdapter extends RecyclerView.Adapter<RecyclerViewChildAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<DashBoardVehicle_POJO> dashBoardVehicle_pojoArrayList;
    View view;

    public RecyclerViewChildAdapter(Context mContext, ArrayList<DashBoardVehicle_POJO> dashBoardVehicle_pojoArrayList) {
        this.mContext = mContext;
        this.dashBoardVehicle_pojoArrayList = dashBoardVehicle_pojoArrayList;
        Log.i(Constants.Tag,"dashBoardVehicle_pojoArrayList : "+dashBoardVehicle_pojoArrayList.size());
        for (int i=0;i<dashBoardVehicle_pojoArrayList.size();i++)
        Log.i(Constants.Tag,"getVehicleDetail().size() : "+dashBoardVehicle_pojoArrayList.get(i).getVehicleDetail().size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_singletextview, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final DashBoardVehicle_POJO pojo = dashBoardVehicle_pojoArrayList.get(i);
        Log.i(Constants.Tag,"dashBoardVehicle_pojoArrayList : "+pojo.getVehicleDetail().size());
        viewHolder.textView.setText(pojo.getVehicleDetail().get(i));
    }

    @Override
    public int getItemCount() {
        return dashBoardVehicle_pojoArrayList./*get(position).getVehicleDetail().*/size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textvw);
        }
    }
}
