package com.example.safeauto.PolicyDetails.model;

public class Vehicle_POJO {
    private String vehicle_details,vehicle_no;

    public Vehicle_POJO() {
    }

    public Vehicle_POJO(String vehicle_details, String vehicle_no) {
        this.vehicle_details = vehicle_details;
        this.vehicle_no = vehicle_no;
    }

    public String getVehicle_details() {
        return vehicle_details;
    }

    public void setVehicle_details(String vehicle_details) {
        this.vehicle_details = vehicle_details;
    }

    public String getVehicle_no() {
        return vehicle_no;
    }

    public void setVehicle_no(String vehicle_no) {
        this.vehicle_no = vehicle_no;
    }
}
