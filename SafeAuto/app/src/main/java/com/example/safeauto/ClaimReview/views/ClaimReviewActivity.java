package com.example.safeauto.ClaimReview.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.safeauto.DashBoard.views.DashBoardActivity;
import com.example.safeauto.PolicyDetails.views.PolicyDetialsActivity;
import com.example.safeauto.R;
import com.example.safeauto.main.views.MainActivity;

public class ClaimReviewActivity extends MainActivity {


    RelativeLayout layout_main;
    View view_child;
    TextView screentitle_txtvw;
    Button button1,button2;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout_main = (RelativeLayout) findViewById(R.id.rellay_main);
        view_child = getLayoutInflater().inflate(R.layout.activity_claim_review, null);
        layout_main.addView(view_child);
        initView();
    }

    public void initView(){
        //screen title modification
        findViewById(R.id.sa_logo).setVisibility(View.GONE);
        screentitle_txtvw = findViewById(R.id.scrntitle_txtvw);
        screentitle_txtvw.setVisibility(View.VISIBLE);
        screentitle_txtvw.setText("Claim Opened");
        layout_main.setBackgroundColor(getResources().getColor(R.color.colorWhite));


        button1 = view_child.findViewById(R.id.button1);
        button2 = view_child.findViewById(R.id.button2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ClaimReviewActivity.this, DashBoardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ClaimReviewActivity.this, PolicyDetialsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }
}
