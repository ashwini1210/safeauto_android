package com.example.safeauto.PolicyDetails.views;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.safeauto.PolicyDetails.adapter.claimRecycler_Aadapter;
import com.example.safeauto.PolicyDetails.model.Claim_POJO;
import com.example.safeauto.R;
import com.example.safeauto.RaiseClaim.views.RaiseAClaimActivity;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClaimsFragment extends Fragment {
    View rootview;
    RecyclerView recyclerView;
    ArrayList<Claim_POJO> claim_pojoArrayList = new ArrayList<>();
    claimRecycler_Aadapter claimRecycler_aadapter;
    Button raiseclaim_btn;


    public ClaimsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_claims, container, false);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.clm_recyclervw);
        addDataToPojo();
        claimRecycler_aadapter = new claimRecycler_Aadapter(claim_pojoArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(claimRecycler_aadapter);
        raiseclaim_btn = rootview.findViewById(R.id.clm_btn);
        raiseclaim_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getActivity().getApplicationContext(), RaiseAClaimActivity.class);
                startActivity(intent);
            }
        });

        return rootview;
    }

    public void addDataToPojo(){
        claim_pojoArrayList.clear();
        Claim_POJO c, c1, c2;
        c = new Claim_POJO("CLM1032", "Jan 12, 2019", "Approved");
        c1 = new Claim_POJO("CLM1045", "Dec 26, 2018", "Settled");
        c2 = new Claim_POJO("CLM1223", "Nov 15, 2018", "Settled");
        claim_pojoArrayList.add(c);
        claim_pojoArrayList.add(c1);
        claim_pojoArrayList.add(c2);
    }

    public static ClaimsFragment newInstance(int position) {

        Bundle args = new Bundle();

        ClaimsFragment fragment = new ClaimsFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
