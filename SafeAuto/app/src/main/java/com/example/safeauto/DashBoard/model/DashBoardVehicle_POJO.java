package com.example.safeauto.DashBoard.model;

import java.util.ArrayList;
import java.util.List;

public class DashBoardVehicle_POJO {
    private ArrayList<String> VehicleDetail;

    public DashBoardVehicle_POJO(ArrayList<String> vehicleDetail) {
        VehicleDetail = vehicleDetail;
    }

    public ArrayList<String> getVehicleDetail() {
        return VehicleDetail;
    }

    public void setVehicleDetail(ArrayList<String> vehicleDetail) {
        VehicleDetail = vehicleDetail;
    }
}
