package com.example.safeauto.DashBoard.views;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.safeauto.DashBoard.model.DashBoardVehicle_POJO;
import com.example.safeauto.R;
import com.example.safeauto.DashBoard.model.DashBoard_POJO;
import com.example.safeauto.main.views.MainActivity;
import com.example.safeauto.DashBoard.adapter.RecyclerCustomAdapter;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import java.util.ArrayList;
import java.util.List;

public class DashBoardActivity extends MainActivity {

    View view_child;
    CarouselView customCarouselView;
    int NUMBER_OF_PAGES = 2;
    RecyclerView recyclerView_policy;
    RelativeLayout layout_main;
    private ArrayList<DashBoard_POJO> dashBoard_pojoList = new ArrayList<>();
    private RecyclerCustomAdapter recyclerCustomAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layout_main = (RelativeLayout) findViewById( R.id.rellay_main);
        findViewById(R.id.sa_logo).setVisibility(View.GONE);
        findViewById(R.id.scrntitle_txtvw).setVisibility(View.VISIBLE);
        layout_main.setBackground(getResources().getDrawable(R.drawable.main_bgimg));

        view_child = getLayoutInflater().inflate( R.layout.activity_dash_board, null);
        initViews();


        layout_main.addView(view_child);

    }

    public void initViews(){

        //screen title modification
        findViewById(R.id.scrntitle_txtvw).setVisibility(View.GONE);
        findViewById(R.id.sa_logo).setVisibility(View.VISIBLE);

        customCarouselView = (CarouselView) view_child.findViewById(R.id.offers_carousel);
        recyclerView_policy = (RecyclerView) view_child.findViewById(R.id.recyclevw);

        dashBoard_pojoList.clear();

        for (int i=0;i<2;i++){
            DashBoard_POJO pojo = new DashBoard_POJO("SF294AK885");
            dashBoard_pojoList.add(pojo);
        }

        recyclerCustomAdapter = new RecyclerCustomAdapter(DashBoardActivity.this,dashBoard_pojoList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView_policy.setLayoutManager(mLayoutManager);
        recyclerView_policy.setItemAnimator(new DefaultItemAnimator());
        recyclerView_policy.setAdapter(recyclerCustomAdapter);

        customCarouselView.setPageCount(NUMBER_OF_PAGES);

        ViewListener viewListener = new ViewListener() {
            @Override
            public View setViewForPosition(int position) {
                View customView = getLayoutInflater().inflate(R.layout.carousel_viewinflate, null);
                //set view attributes here
                return customView;
            }
        };

        // set ViewListener for custom view
        customCarouselView.setViewListener(viewListener);
    }

}
