package com.example.safeauto.PolicyDetails.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.safeauto.PolicyDetails.model.Driver_POJO;
import com.example.safeauto.R;
import com.example.safeauto.utils.Constants;

import java.util.ArrayList;

public class driverRecycler_Adapter extends RecyclerView.Adapter<driverRecycler_Adapter.ViewHolder> {
    private  ArrayList<Driver_POJO> driver_pojoArrayList;
    private Context mContext;

    public driverRecycler_Adapter(ArrayList<Driver_POJO> driver_pojoArrayList, Context mContext) {
        this.driver_pojoArrayList = driver_pojoArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_drivers, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull driverRecycler_Adapter.ViewHolder viewHolder, int i) {
        Driver_POJO pojo = driver_pojoArrayList.get(i);
        viewHolder.drv_name.setText(pojo.getDrv_name());
        viewHolder.drv_dob.setText(pojo.getDrv_dob());
        viewHolder.drv_license.setText(pojo.getDrv_license());
        Log.i(Constants.Tag,"image resource : "+pojo.getDrv_imgsrc());
        Uri uri = (Uri) Constants.resourceIdToUri(mContext, pojo.getDrv_imgsrc());
        Glide.with(mContext).load(uri).placeholder(R.mipmap.dvrimg_1).into(viewHolder.drv_img);

    }

    @Override
    public int getItemCount() {
        return driver_pojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView drv_name, drv_dob, drv_license;
        ImageView drv_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            drv_name = (TextView) itemView.findViewById(R.id.dr_name);
            drv_dob = (TextView) itemView.findViewById(R.id.dr_dob);
            drv_license = (TextView) itemView.findViewById(R.id.dr_license);
            drv_img = (ImageView) itemView.findViewById(R.id.dr_img);
        }

        @Override
        public void onClick(View v) {

        }
    }


}
