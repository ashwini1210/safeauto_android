package com.example.safeauto.PolicyDetails.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.safeauto.PolicyDetails.model.Vehicle_POJO;
import com.example.safeauto.R;
import java.util.ArrayList;


public class vehicleRecycler_Adapter extends RecyclerView.Adapter<vehicleRecycler_Adapter.ViewHolder> {

    private ArrayList<Vehicle_POJO> vehicle_pojoList;

    public vehicleRecycler_Adapter(ArrayList<Vehicle_POJO> vehicle_pojoList) {
        this.vehicle_pojoList = vehicle_pojoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_vehicles, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Vehicle_POJO pojo = vehicle_pojoList.get(i);
        viewHolder.vh_details.setText(pojo.getVehicle_details());
        viewHolder.vh_no.setText(pojo.getVehicle_no());
    }

    @Override
    public int getItemCount() {
        return vehicle_pojoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView vh_details,vh_no;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            vh_details = (TextView) itemView.findViewById(R.id.vh_info);
            vh_no = (TextView) itemView.findViewById(R.id.vh_policyinfo);
        }

        @Override
        public void onClick(View v) {
        }
    }
}
