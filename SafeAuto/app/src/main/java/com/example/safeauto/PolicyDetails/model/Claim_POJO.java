package com.example.safeauto.PolicyDetails.model;

public class Claim_POJO {
    private String clm_no, clm_date, clm_status;

    public Claim_POJO(String clm_no, String clm_date, String clm_status) {
        this.clm_no = clm_no;
        this.clm_date = clm_date;
        this.clm_status = clm_status;
    }

    public String getClm_no() {
        return clm_no;
    }

    public void setClm_no(String clm_no) {
        this.clm_no = clm_no;
    }

    public String getClm_date() {
        return clm_date;
    }

    public void setClm_date(String clm_date) {
        this.clm_date = clm_date;
    }

    public String getClm_status() {
        return clm_status;
    }

    public void setClm_status(String clm_status) {
        this.clm_status = clm_status;
    }
}
