package com.example.safeauto.utils;

import android.content.Context;


import android.graphics.Typeface;
import android.net.Uri;
import com.example.safeauto.PolicyDetails.model.Driver_POJO;
import com.example.safeauto.PolicyDetails.model.Vehicle_POJO;
import com.example.safeauto.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class Constants {

    public static final String Tag = "SafeAuto";
    public static String POLICY_NO = "SafeAuto";

    public static final String ANDROID_RESOURCE = "android.resource://";
    public static final String FOREWARD_SLASH = "/";
    public static final int REQUEST_PICK_IMAGE = 1001;
    public static final int REQUEST_CAMERA = 1000;
    public static boolean RFlag = true;
    public static final ArrayList<Vehicle_POJO> vehicle_pojoArrayList = new ArrayList<>();
    public static final ArrayList<Driver_POJO> driver_pojoArrayList = new ArrayList<>();

    public static Uri resourceIdToUri(Context context, int resourceId) {
        return Uri.parse(ANDROID_RESOURCE + context.getPackageName() + FOREWARD_SLASH + resourceId);
    }

    public static final void addDatatoVehiclePojo(){
        vehicle_pojoArrayList.clear();
        Vehicle_POJO v,v1,v2;
        v = new Vehicle_POJO("2018 | BMW | Z4","LJCPCBLCX11000232");
        vehicle_pojoArrayList.add(v);
        v1 = new Vehicle_POJO("2017 | Mercedes-Benz | 4Matic GLC","LJCPCBLCX87449823");
        vehicle_pojoArrayList.add(v1);
        v2 = new Vehicle_POJO("2016 | Audi | A4","LJCPCBLCX98263746");
        vehicle_pojoArrayList.add(v2);
    }

    public static final void addDataToDriverPojo(){
        driver_pojoArrayList.clear();
        Driver_POJO d,d1,d2;
        d = new Driver_POJO("Ted Lawson", "11-09-1970", "NJ 123456789", R.mipmap.dvrimg_1);
        driver_pojoArrayList.add(d);
        d1 = new Driver_POJO("Jimmy Lawson", "12-01-1987", "NJ 987654321",R.mipmap.dvrimg_2);
        driver_pojoArrayList.add(d1);
        d2 = new Driver_POJO("Martin Lawson", "16-06-1990", "NJ 897867342", R.mipmap.dvrimg_3);
        driver_pojoArrayList.add(d2);

    }


}
