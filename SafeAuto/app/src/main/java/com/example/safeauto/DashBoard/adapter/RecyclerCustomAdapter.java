package com.example.safeauto.DashBoard.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.safeauto.DashBoard.model.DashBoardVehicle_POJO;
import com.example.safeauto.PolicyDetails.views.PolicyDetialsActivity;
import com.example.safeauto.R;
import com.example.safeauto.DashBoard.model.DashBoard_POJO;
import com.example.safeauto.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class RecyclerCustomAdapter extends RecyclerView.Adapter<RecyclerCustomAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<DashBoard_POJO> dashBoard_pojoList;
    private ArrayList<String> vehical_list = new ArrayList<>();
    private ArrayList<DashBoardVehicle_POJO> db_pojoList = new ArrayList<>();
    RecyclerViewChildAdapter recyclerViewChildAdapter;

    public RecyclerCustomAdapter(Context mContext, ArrayList<DashBoard_POJO> dashBoard_pojoList) {
        this.mContext = mContext;
        this.dashBoard_pojoList = dashBoard_pojoList;
        addDataToPojo();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.policy_recycler_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final DashBoard_POJO pojo = dashBoard_pojoList.get(i);
        viewHolder.policy_no.setText(pojo.getPolicy_Number());

        recyclerViewChildAdapter = new RecyclerViewChildAdapter(mContext, db_pojoList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        ((LinearLayoutManager) mLayoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        viewHolder.childRecyclvw.setLayoutManager(mLayoutManager);
        viewHolder.childRecyclvw.setAdapter(recyclerViewChildAdapter);

//        viewHolder.policy_status.setText(pojo.getPolicy_Number());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PolicyDetialsActivity.class);
                /*intent.putExtra("Policy_No", pojo.getPolicy_Number());*/
                Constants.POLICY_NO = pojo.getPolicy_Number();
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dashBoard_pojoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView policy_no, policy_status;
        private RecyclerView childRecyclvw;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            childRecyclvw = (RecyclerView) itemView.findViewById(R.id.policyno_recycler);
            policy_no = (TextView) itemView.findViewById(R.id.policyno_txt);
//            policy_status = (TextView) itemView.findViewById(R.id.policystatus_txt);
        }

    }

    public void addDataToPojo() {
        vehical_list.clear();
        db_pojoList.clear();

        vehical_list.add("2018-BMW-Z4");
        vehical_list.add("2017-Merc-4matic");

        DashBoardVehicle_POJO db_pojo = new DashBoardVehicle_POJO(vehical_list);
        db_pojoList.add(db_pojo);

        Log.i(Constants.Tag, "vehical_list" + vehical_list.get(0));

//        Toast.makeText(mContext, "vehical_list :" + vehical_list.size() + "\n db_pojoList : " + db_pojoList.size(), Toast.LENGTH_SHORT).show();

        vehical_list.add("2017-Audi-A4");
        DashBoardVehicle_POJO db_pojo1 = new DashBoardVehicle_POJO(vehical_list);
        db_pojoList.add(db_pojo1);
    }

}
