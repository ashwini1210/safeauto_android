package com.example.safeauto.RaiseClaim.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.safeauto.PolicyDetails.model.Driver_POJO;
import com.example.safeauto.R;
import com.example.safeauto.utils.Constants;

import java.util.ArrayList;

public class SelectDriverAdapter extends RecyclerView.Adapter<SelectDriverAdapter.ViewHolder> {

    ArrayList<Driver_POJO> pojoArrayList = new ArrayList<>();
    private int lastSelectedPosition = -1;
    private Context mContext;

    public SelectDriverAdapter(ArrayList<Driver_POJO> pojoArrayList, Context mContext) {
        this.pojoArrayList = pojoArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_drvradiobtn, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Driver_POJO pojo = pojoArrayList.get(i);
        viewHolder.drv_txtview1.setText(pojo.getDrv_name());
        viewHolder.drv_txtview2.setText(pojo.getDrv_dob());
        viewHolder.drv_txtview3.setText(pojo.getDrv_license());
        Log.i(Constants.Tag,"image resource : "+pojo.getDrv_imgsrc());
        Uri uri = (Uri) Constants.resourceIdToUri(mContext, pojo.getDrv_imgsrc());
        Glide.with(mContext).load(uri).placeholder(R.mipmap.dvrimg_1).into(viewHolder.drv_img);
        viewHolder.drv_radiobtn.setChecked(lastSelectedPosition == i);

    }

    @Override
    public int getItemCount() {
        return pojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView drv_txtview1, drv_txtview2, drv_txtview3;
        public RadioButton drv_radiobtn;
        ImageView drv_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            drv_txtview1 = (TextView) itemView.findViewById(R.id.drv_name);
            drv_txtview2 = (TextView) itemView.findViewById(R.id.drv_dob);
            drv_txtview3 = (TextView) itemView.findViewById(R.id.drv_license);
            drv_radiobtn = (RadioButton) itemView.findViewById(R.id.drv_radiobtn);
            drv_img = (ImageView) itemView.findViewById(R.id.drv_img);
            drv_radiobtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    Toast.makeText(SelectDriverAdapter.this.mContext,"selected driver position : "+lastSelectedPosition,Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
