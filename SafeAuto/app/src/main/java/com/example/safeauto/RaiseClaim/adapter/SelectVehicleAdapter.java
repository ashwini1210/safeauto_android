package com.example.safeauto.RaiseClaim.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.safeauto.PolicyDetails.model.Vehicle_POJO;
import com.example.safeauto.R;
import com.example.safeauto.RaiseClaim.views.RaiseAClaimActivity;

import java.util.ArrayList;

public class SelectVehicleAdapter extends RecyclerView.Adapter<SelectVehicleAdapter.ViewHolder> {
    ArrayList<Vehicle_POJO> pojoArrayList = new ArrayList<>();
    private Context mContext;
    private int lastSelectedPosition = -1;

    public SelectVehicleAdapter(ArrayList<Vehicle_POJO> pojoArrayList, Context mContext) {
        this.pojoArrayList = pojoArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_vhradiobtn, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Vehicle_POJO pojo = pojoArrayList.get(i);
        viewHolder.vh_txtview1.setText(pojo.getVehicle_details());
        viewHolder.vh_txtveiw2.setText(pojo.getVehicle_no());

        viewHolder.vh_radiobtn.setChecked(lastSelectedPosition == i);
    }

    @Override
    public int getItemCount() {
        return pojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView vh_txtview1,vh_txtveiw2;
        RadioButton vh_radiobtn;
        CheckBox vh_cbbtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            vh_txtview1 = (TextView) itemView.findViewById(R.id.vh_txtvw1);
            vh_txtveiw2 = (TextView) itemView.findViewById(R.id.vh_txtvw2);

            vh_radiobtn = (RadioButton) itemView.findViewById(R.id.vh_radiobtn);

            vh_cbbtn = (CheckBox) itemView.findViewById(R.id.vh_cbbtn);

            if (RaiseAClaimActivity.Flag == true){
                vh_cbbtn.setVisibility(View.VISIBLE);
                vh_radiobtn.setVisibility(View.GONE);
            }
            else if(RaiseAClaimActivity.Flag == false){
                vh_cbbtn.setVisibility(View.GONE);
                vh_radiobtn.setVisibility(View.VISIBLE);
            }

            vh_radiobtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }
}
