package com.example.safeauto.RaiseClaim.Model;

import android.graphics.Bitmap;

public class ImageModel {

    private Bitmap photo;

    public ImageModel() {
    }

    public ImageModel(Bitmap photo) {
        this.photo = photo;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }
}
