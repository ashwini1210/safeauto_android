package com.example.safeauto.PolicyDetails.model;

public class Coverage_POJO {

    private String cv_lib, cv_accdnt, cv_persn;



    public Coverage_POJO(String cv_lib, String cv_accdnt, String cv_persn) {
        this.cv_lib = cv_lib;
        this.cv_accdnt = cv_accdnt;
        this.cv_persn = cv_persn;
    }


    public String getCv_lib() {
        return cv_lib;
    }

    public void setCv_lib(String cv_lib) {
        this.cv_lib = cv_lib;
    }

    public String getCv_accdnt() {
        return cv_accdnt;
    }

    public void setCv_accdnt(String cv_accdnt) {
        this.cv_accdnt = cv_accdnt;
    }

    public String getCv_persn() {
        return cv_persn;
    }

    public void setCv_persn(String cv_persn) {
        this.cv_persn = cv_persn;
    }
}
