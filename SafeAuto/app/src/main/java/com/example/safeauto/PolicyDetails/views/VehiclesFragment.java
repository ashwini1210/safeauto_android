package com.example.safeauto.PolicyDetails.views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.safeauto.PolicyDetails.adapter.vehicleRecycler_Adapter;
import com.example.safeauto.PolicyDetails.model.Vehicle_POJO;
import com.example.safeauto.R;
import com.example.safeauto.utils.Constants;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiclesFragment extends Fragment {
    View rootview;
    RecyclerView vhrecycleView;
    vehicleRecycler_Adapter vehicleRecycler_adapter;

    public VehiclesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_vehicles, container, false);
        vhrecycleView = (RecyclerView) rootview.findViewById(R.id.vh_recyclervw);
        Constants.addDatatoVehiclePojo();
        vehicleRecycler_adapter = new vehicleRecycler_Adapter(Constants.vehicle_pojoArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        vhrecycleView.setLayoutManager(mLayoutManager);
        vhrecycleView.setAdapter(vehicleRecycler_adapter);
        return rootview;
    }

    public static VehiclesFragment newInstance(int position) {
        Bundle args = new Bundle();
        VehiclesFragment fragment = new VehiclesFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
