# Safe Auto

## Summery

Start your Insurance policy in just a few minutes with a low down payment and affordable monthly installments.


## Version

This app is developed for Android 


## Configuring the Project

Clone this repository and import into Android Studio

1. Open QuizApp in the Android studio.
2. Run an App on the simulator or device


## Features:

1. Signup with Facebook or Gmail.
2. Pay your bill, view your proof of insurance, and seamlessly access your insurance documents online.
3. Get Notification once insurance is going to expire
4. History of complete insurance
